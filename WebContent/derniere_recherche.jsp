<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="Controller.*,java.util.*,Model.*"%>

<%!
	public ArrayList<Produit> listeProduit = new ArrayList<Produit>();	;
%>

<%
	session = request.getSession();	
	
	String liste = "Aucun produit precédemment recherché !";
	
	if(session.getAttribute("liste") != null){
		listeProduit = (ArrayList<Produit>)session.getAttribute("liste");
		if(!listeProduit.isEmpty())
			liste = "";
		System.out.println(listeProduit.toString());
	}
%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- link Bulma, Bootstrap, Buefy, AOS, FontAwesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito|Cookie|Quando|Quattrocento|Quintessential" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    
    <title>Open Food Fact</title>
</head>

<body>

    <!-- Corps de la page -->
    <div class="container">
    	<div class="column">
    	
    	 	<!-- Barre de Navigation -->
    		<nav class="navbar navbar-light bg-primary">
			  	<a class="navbar-brand" id="nav-brand" href="index.jsp"><h3>Open Food Fact</h3></a>
			  	
			  	<div class=" d-flex flex-row justify-content-left">
			  		<div class="field has-addons">
					  	<div class="control">
					    	<input class="input" type="text" placeholder="recherche par nom">
					  	</div>
					  	<div class="control">
					    	<a class="button is-info">GO</a>
					    </div>
					</div>
					
					<div class="ml-5 field has-addons">
					  	<div class="control">
					    	<input class="input" type="text" placeholder="recherche par ingredients">
					  	</div>
					  	<div class="control">
					    	<a class="button is-info">GO</a>
					    </div>
					</div>
			  	</div>
			  		
			</nav>
    	</div>
    	
    	<div class="column"> 
		
		<%
			if(liste == ""){%>
				<h3 id="pre"> Précédemment recherchés </h3>	
		<%		
				for(int i = 0; i < listeProduit.size(); i++){
					String url = listeProduit.get(i).getImage_url();
					String name ;
					String brand;
					String mass;
					String id =  listeProduit.get(i).getId() ;
					
					if(listeProduit.get(i).getNom().equals("")){
						name = "Valeur Inexistante";
					}else{
						name = listeProduit.get(i).getNom();
					}
				
					if(listeProduit.get(i).getMasse().equals("")){
						mass = "Valeur Inexistante";
					}else{
						mass = listeProduit.get(i).getMasse();
					}
					
					if(listeProduit.get(i).getMarque().equals("")){
						brand = "Valeur inexistante";
					}else{
						brand = listeProduit.get(i).getMarque();
					}
		 %>
								
					<div class="column"> 
						<div class="box">
							<div class="columns carte">
								<div clas="column" id="img"> 
									<figure id="figure" class="image is-128x128">
									  	<img id="img" src= <%=url %> >
									</figure>
								</div>
								<div clas="column" id="desc">
									<div class="column" id="name"> <strong><%=name %></strong> </div>
									<div class="column"> Brand : <%=brand  %></div>
									<div class="column" id="masse"> Masse :  <%=mass  %></div>
								</div>
								<div clas="column"> 
									<div id="id"> <p><%=id %></p> </div>
								</div>
							</div>
						</div>
					</div>
					
				<%}
			}else{%>
				<h3><%=liste %></h3>
			<%}
		%>
		
    	
    	</div>
    </div>

       

    <!-- Bootstrap, Buefy and AOS JS requirements -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>AOS.init();</script>
</body>


</html>