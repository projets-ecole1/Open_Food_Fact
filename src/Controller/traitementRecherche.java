package Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.Produit;

/**
 * Servlet implementation class traitementRecherche
 */
@WebServlet("/traitementRecherche")
public class traitementRecherche extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public traitementRecherche() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Cookie error = null;
		/* Cr�ation ou r�cup�ration de la session */
		HttpSession session = request.getSession();
		 
		PrintWriter out = response.getWriter();
		DAO dao = new DAO();
		String Name = "";
		
		//Recupere le parametre du produit si il n'est pas nul
		if(!request.getParameter("Nom").isEmpty()) {
			Name = request.getParameter("Nom");
		}
		
		// Si il est vide, on effectue une redirection
		if(Name == "") {
			error = new Cookie("error", "empty");
			error.setMaxAge(-1);
			response.addCookie(error);
			response.sendRedirect("index.jsp");
			
		}else {
			
			// Si le produit est inexistant on effectue egalement une redirection vers la page d'acceuil
			if(dao.getProduitByIng(Name).isEmpty() && dao.getProduitByName(Name).isEmpty()) {
				error = new Cookie("error", "false");
				error.setMaxAge(-1); //this is a session cookie
				response.addCookie(error);
				response.sendRedirect("index.jsp");
				
			}else {
				
				/* Mise en session du produit */
				ArrayList<Produit> listeProduit = (ArrayList<Produit>) session.getAttribute("liste");
				ArrayList<Produit> recherche = (ArrayList<Produit>) session.getAttribute("recherche");
				
				/*1st step: On effectue le recherche du produit par nom*/
				recherche = dao.getProduitByName(Name);
				
				// Et si il n'existe pas on effectue par ingredients
				if(recherche.isEmpty()) {
					recherche = dao.getProduitByIng(Name);
				}
				
				// Mise en session de l'objet retrouv� si non vide
				session.setAttribute( "recherche", recherche );
				
				// Ajout unique des �l�ments de la recherche dans une liste consultable dans les recherches prec�dentes
				for(int i = 0; i < recherche.size(); i++) {
					listeProduit = AjoutUnique(listeProduit, recherche.get(i));
				}	
				
				// Mise en session de la liste
				session.setAttribute( "liste", listeProduit );
				response.sendRedirect("resultat_recherche.jsp");
				
			}
		}
		
	}
	
	/**
	 * Methode Ajout Unique qui permet de remplir une arraylist avec des produits uniques. 
	 * Ils sont rajout�s si ils n'existent pas, ignor�s sinon
	 * 
	 * @param un Produit et une Arraylist
	 * @return L'arraylist modifi�e
	 */
	public ArrayList<Produit> AjoutUnique(ArrayList<Produit> l, Produit p){
		boolean find = false;
		int i = 0;
		
		while(i<l.size() && !find) {
			
			if(p.getId().equals(l.get(i).getId())) {
				find = true;
				break;
			}else {
				i++;
			}	
		}
		
		if(!find) {
			l.add(p);
		}
		
		return l;
	}
}
