package Model;
import java.util.ArrayList;
import java.util.List;

public class Produit {

	private String nom;
	private String masse;
	private String id;
	private String marque;
	private String image_url;
	private String auteur;
	private String ingredient;
	private String img_ing_url;
	private String nut_ing_url;
	
	/**
	 * @param nom
	 * @param masse
	 * @param id
	 * @param marque
	 * @param image_url
	 * @param auteur
	 * @param ingredient
	 */
	public Produit(String nom, String masse, String id, String marque, String image_url, String auteur,String ingredient,String img_ing_url, String nut_ing_url) {
	
		this.nom = nom;
		this.masse = masse;
		this.id = id;
		this.marque = marque;
		this.image_url = image_url;
		this.auteur = auteur;
		this.ingredient = ingredient;
		this.img_ing_url = img_ing_url;
		this.nut_ing_url = nut_ing_url;
	}
	
	public Produit() {
		
		this.nom = "NULL";
		this.masse = "NULL";
		this.id = "NULL";
		this.marque = "NULL";
		this.image_url = "NULL";
		this.auteur = "NULL";
		this.ingredient = "NULL";
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @return the masse
	 */
	public String getMasse() {
		return masse;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the marque
	 */
	public String getMarque() {
		return marque;
	}

	/**
	 * @return the image_url
	 */
	public String getImage_url() {
		return image_url;
	}

	/**
	 * @return the auteur
	 */
	public String getAuteur() {
		return auteur;
	}

	/**
	 * @return the ingredient
	 */
	public String getIngredient() {
		return ingredient;
	}
	
	
	public String getImg_ing_url() {
		return img_ing_url;
	}

	public void setImg_ing_url(String img_ing_url) {
		this.img_ing_url = img_ing_url;
	}

	public String getNut_ing_url() {
		return nut_ing_url;
	}

	public void setNut_ing_url(String nut_ing_url) {
		this.nut_ing_url = nut_ing_url;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "\nProduit [nom=" + nom + ", masse=" + masse + ", id=" + id + ", marque=" + marque + ", image_url="
				+ image_url + ", auteur=" + auteur + ", ingredient=" + ingredient + "]\n";
	}
	

	public static void main(String[] args) {
	
	}
	
}

